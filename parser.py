#!/usr/bin/env python3
from urllib.request import urlopen
from urllib.parse import urljoin

from lxml.html import fromstring
import xlsxwriter

URL = 'https://geekbrains.ru/courses'
#ITEM_PATH = '.profession-card__title'
ITEM_PATH = 'div.gb-profession-cards-grid__item'


def parse_courses() :
	f = urlopen( URL )
	list_html = f.read().decode( encoding='utf_8' )
	list_doc = fromstring( list_html )

	courses = []

	for elem in list_doc.cssselect( ITEM_PATH ) :
		cat_name = elem.cssselect( '.gb-profession-cards-grid__title' )[0].text
		for a_tag in elem.cssselect( 'a' ) :
			link_text = a_tag.get( 'href' )
			title_elem = a_tag.cssselect( '.profession-card__title' )[0].text
			print( cat_name, link_text, title_elem )
			url = urljoin( URL , link_text )

			details_html = urlopen( url ).read().decode( 'utf-8' )
			detail_doc = fromstring( details_html )
			d = { 'cat' : cat_name, 'title' : title_elem, 'url' : url }
			courses.append( d )

	return courses


def export_excel( filename, courses ) :
	workbook = xlsxwriter.Workbook( filename )
	worksheet = workbook.add_worksheet()

	bold = workbook.add_format( { 'bold' : True } )
	title_list = ['Категория', 'Название', 'Ссылка'  ]

	#worksheet.write( 0, 0, 'Hello, World!', bold )
	for i, title in enumerate( title_list ) :
		worksheet.write( 0, i, title, bold )

	for i, c in enumerate( courses, start=1 ) :
		worksheet.write( i, 0, c['cat'] )
		worksheet.write( i, 1, c['title'] )
		worksheet.write( i, 2, c['url'] )


	workbook.close()

def main() :
	courses = parse_courses()
	#courses = []
	export_excel( 'export.xlsx' , courses)

if __name__ == '__main__':
	main()